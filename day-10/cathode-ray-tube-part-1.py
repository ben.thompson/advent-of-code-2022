operations = open("input.txt", "r").read().splitlines()

cycle = 1
cycles = [ 20, 60, 100, 140, 180, 220 ]
X = 1
result = 0
for operation in operations:
    if cycle in cycles:
        result += X * cycle
    cycle += 1
    if operation != "noop":
        if cycle in cycles:
            result += X * cycle
        X += int(operation.split()[1])
        cycle += 1

print(result)