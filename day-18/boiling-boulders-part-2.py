import math

boulders = open("input.txt", "r").read().splitlines()

def main():
    max = findMax()
    grid = [ [ ["." for i in range(max + 2)] for i in range(max + 2)] for i in range(max + 2) ]
    grid = fillGrid(grid)
    airPockets = findAirPockets(grid)
    print(countSides(grid, airPockets))

def findMax():
    max = 0
    for boulder in boulders:
        x, y, z = [int(i) for i in boulder.split(",")]
        if x > max:
            max = x
        if y > max:
            max = y
        if z > max:
            max = z
    return max

def fillGrid(grid):
    for boulder in boulders:
        x, y, z = [int(i) for i in boulder.split(",")]
        grid[x][y][z] = "#"
    return grid

def countSides(grid, airPockets):
    totalSides = 0
    for i in range(1, len(grid) - 1):
        for j in range(1, len(grid[i]) - 1):
            for k in range(1, len(grid[i][j]) - 1):
                if grid[i][j][k] == "#":
                    if grid[i + 1][j][k] == "." and (i + 1, j, k) not in airPockets:
                        totalSides += 1
                    if grid[i - 1][j][k] == "." and (i - 1, j, k) not in airPockets:
                        totalSides += 1
                    if grid[i][j + 1][k] == "." and (i, j + 1, k) not in airPockets:
                        totalSides += 1
                    if grid[i][j - 1][k] == "." and (i, j - 1, k) not in airPockets:
                        totalSides += 1
                    if grid[i][j][k + 1] == "." and (i, j, k + 1) not in airPockets:
                        totalSides += 1
                    if grid[i][j][k - 1] == "." and (i, j, k - 1) not in airPockets:
                        totalSides += 1
    return totalSides

def findAirPockets(grid):
    airPockets = []
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            for k in range(len(grid[i][j])):
                if grid[i][j][k] == ".":
                    isFound = False
                    for airPocket in airPockets:
                        for airCube in airPocket:
                            if isAdjascent(airCube, (i, j, k)):
                                airPocket.append((i, j, k))
                                isFound = True
                                break
                        if isFound:
                            break
                    if not isFound:
                        airPockets.append([(i, j, k)])
    i = 0
    totalRuns = 0
    maxRuns = int(math.log2(len(airPockets))) + 1
    while totalRuns < maxRuns:
        j = i + 1
        foundAdjascent = False
        while j < len(airPockets):
            for k in airPockets[i]:
                for l in airPockets[j]:
                    if isAdjascent(k, l):
                        airPockets[i] += airPockets[j]
                        airPockets.pop(j)
                        foundAdjascent = True
                        break
                if foundAdjascent:
                    break
            j += 1
        i += 1
        if i == len(airPockets) - 1:
            i = 0
            totalRuns += 1
    for i in range(len(airPockets)):
        if (1, 1, 1) in airPockets[i]:
            airPockets.pop(i)
            break
    result = []
    for airPocket in airPockets:
        result += airPocket
    return result

def isAdjascent(first, second):
    return (( abs(first[0] - second[0]) == 1 and first[1] == second[1] and first[2] == second[2] ) or 
            ( first[0] == second[0] and abs(first[1] - second[1]) == 1 and first[2] == second[2] ) or
            ( first[0] == second[0] and first[1] == second[1] and abs(first[2] - second[2]) == 1 ))

main()