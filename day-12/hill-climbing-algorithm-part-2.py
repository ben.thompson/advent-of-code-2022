hill = open("input.txt", "r").read().splitlines()

def findBestPath():
    bestPath = 1000
    for row in range(len(hill)):
        for col in range(len(hill[row])):
            if hill[row][col] == "a" or hill[row][col] == "S":
                steps = findPath([row, col])
                if steps < bestPath:
                    bestPath = steps
    return bestPath

def findPath(start):
    numSteps = 0
    alreadyTried = [ start ]
    paths = [ [start] ]
    while not isComplete(paths) and numSteps <= 100000:
        numSteps += 1
        newPaths = []
        for path in paths:
            if isTestable(path[-1], (1, 0), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0] + 1, path[-1][1]])
                alreadyTried.append([path[-1][0] + 1, path[-1][1]])
                newPaths.append(newPath)
            if isTestable(path[-1], (0, 1), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0], path[-1][1] + 1])
                alreadyTried.append([path[-1][0], path[-1][1] + 1])
                newPaths.append(newPath)
            if isTestable(path[-1], (-1, 0), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0] - 1, path[-1][1]])
                alreadyTried.append([path[-1][0] - 1, path[-1][1]])
                newPaths.append(newPath)
            if isTestable(path[-1], (0, -1), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0], path[-1][1] - 1])
                alreadyTried.append([path[-1][0], path[-1][1] - 1])
                newPaths.append(newPath)
        paths = newPaths.copy()
    return numSteps

def isComplete(paths):
    for path in paths:
        if hill[path[-1][0]][path[-1][1]] == "E":
            return True
    return False

def isTestable(location, movement, alreadyTried):
    return (
            location[0] + movement[0] >= 0 and location[1] + movement[1] >= 0 and
            len(hill) > location[0] + movement[0] and len(hill[0]) > location[1] + movement[1] and
            [location[0] + movement[0], location[1] + movement[1]] not in alreadyTried and
            isSteppable([location[0], location[1]], [location[0] + movement[0], location[1] + movement[1]])
        )

def isSteppable(location, destination):
    dest = hill[destination[0]][destination[1]]
    if hill[destination[0]][destination[1]] == "E":
        dest = "z"
    source = hill[location[0]][location[1]]
    if hill[location[0]][location[1]] == "S":
        source = "a"
    return ord(dest) - ord(source) >= -10 and ord(dest) - ord(source) <= 1


print(findBestPath())