dataStream = open("input.txt", "r").read()

for i in range(len(dataStream) - 4):
    sequence = dataStream[i:i+4]
    if len(set(sequence)) == len(sequence):
        print(i+4)
        break