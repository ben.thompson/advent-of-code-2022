file = open("input.txt", "r")
lines = file.readlines()
points = 0
for line in lines:
    opponent = line[0]
    me = line[2]
    if me == "X":
        points += 1
        if opponent == "A":
            points += 3
        elif opponent == "C":
            points += 6
    elif me == "Y":
        points += 2
        if opponent == "A":
            points += 6
        elif opponent == "B":
            points += 3
    else:
        points += 3
        if opponent == "B":
            points += 6
        elif opponent == "C":
            points += 3

print(points)
