operations = open("input.txt", "r").read().splitlines()

cycle = 1
X = 1
print("#", end='')
for operation in operations:
    if cycle == 240:
        break
    if abs(cycle % 40 - X) <= 1:
        print("#", end='')
    else:
        print(".", end='')
    cycle += 1
    if cycle % 40 == 0:
        print()
    if operation != "noop":
        X += int(operation.split()[1])
        if abs(cycle % 40 - X) <= 1:
            print("#", end='')
        else:
            print(".", end='')
        cycle += 1
        if cycle % 40 == 0:
            print()