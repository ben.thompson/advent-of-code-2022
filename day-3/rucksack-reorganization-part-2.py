def getPriority(letter):
    if ord(letter) > 96:
        return ord(letter) - 96
    else:
        return ord(letter) - 38

rucksacks = open("input.txt", "r").read().splitlines()
totalPriority = 0
for i in range(0, len(rucksacks), 3):
    for letter in rucksacks[i]:
        if letter in rucksacks[i+1] and letter in rucksacks[i+2]:
            totalPriority += getPriority(letter)
            break

print(totalPriority)