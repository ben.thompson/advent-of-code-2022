assignmentPairs = open("input.txt", "r").read().splitlines()

redundantAssignments = 0
for assignmentPair in assignmentPairs:
    elves = assignmentPair.split(",")
    elfOneSectionIds = elves[0].split("-")
    elfTwoSectionIds = elves[1].split("-")
    elfOneLowId = int(elfOneSectionIds[0])
    elfOneHighId = int(elfOneSectionIds[1])
    elfTwoLowId = int(elfTwoSectionIds[0])
    elfTwoHighId = int(elfTwoSectionIds[1])
    if (elfOneHighId <= elfTwoHighId and elfOneLowId >= elfTwoLowId) or (elfTwoHighId <= elfOneHighId and elfTwoLowId >= elfOneLowId):
        redundantAssignments += 1

print(redundantAssignments)