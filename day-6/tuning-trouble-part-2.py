dataStream = open("input.txt", "r").read()

for i in range(len(dataStream) - 14):
    sequence = dataStream[i:i+14]
    if len(set(sequence)) == len(sequence):
        print(i+14)
        break