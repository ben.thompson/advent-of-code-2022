jets = open("input.txt", "r").read()

class Rock:
    def __init__(self, pattern, startingPosition):
        self.pattern = pattern
        coordinates = []
        for row in range(len(pattern)):
            for col in range(len(pattern[row])):
                if pattern[row][col] == "#":
                    coordinates.append([startingPosition[0] + row, startingPosition[1] + col])
        self.coordinates = coordinates

    def move(self, direction = "v"):
        coordinates = []
        if direction == "<":
            for coordinate in self.coordinates:
                coordinate[1] -= 1
                coordinates.append(coordinate)
        elif direction == ">":
            for coordinate in self.coordinates:
                coordinate[1] += 1
                coordinates.append(coordinate)
        else:
            for coordinate in self.coordinates:
                coordinate[0] -= 1
                coordinates.append(coordinate)
        self.coordinates = coordinates

    def getTopHeight(self, currentHeight):
        topHeight = currentHeight
        for coordinate in self.coordinates:
            if coordinate[0] + 1 > topHeight:
                topHeight = coordinate[0] + 1
        return topHeight

def main():
    rocks = [ ["####"], [".#.", "###", ".#."], ["###", "..#", "..#"], ["#", "#", "#", "#"], ["##", "##"] ]
    currentHeight = 0
    grid = [ ["." for i in range(7)] for i in range(100000) ]
    cache = {}
    jetIndex = 0
    atShortcut = False
    firstPatternIndex = 0
    firstPatternHeight = 0
    secondPatternIndex = 0
    secondPatternHeight = 0
    lastSectionIndex = 0
    i = 0
    while True:
        if (i % 5, jetIndex % len(jets)) in cache and not atShortcut:
            repeatedCache = cache[(i % 5, jetIndex % len(jets))]
            firstPatternIndex = repeatedCache[0]
            firstPatternHeight = repeatedCache[1]
            secondPatternIndex = i
            secondPatternHeight = currentHeight
            lastSectionIndex = secondPatternIndex + (1000000000000 - firstPatternIndex) % (secondPatternIndex - firstPatternIndex)
            atShortcut = True
        else:
            cache[(i % 5, jetIndex % len(jets))] = (i, currentHeight)
        if i == lastSectionIndex and atShortcut:
            lastSectionHeight = currentHeight
            break
        isFalling = True
        rock = Rock(rocks[i % 5], (currentHeight + 3, 2))
        while isFalling:
            jet = jets[jetIndex % len(jets)]
            if canMove(grid, rock, jet):
                rock.move(jet)
            if canMove(grid, rock):
                rock.move()
            else:
                grid = settleRock(grid, rock)
                currentHeight = rock.getTopHeight(currentHeight)
                isFalling = False
            jetIndex += 1
        i += 1
    totalCycles = (1000000000000 - firstPatternIndex) // (secondPatternIndex - firstPatternIndex)
    totalHeight = (secondPatternHeight - firstPatternHeight) * (totalCycles - 1) + lastSectionHeight
    print(totalHeight)

def canMove(grid, rock, direction = "v"):
    if direction == "<":
        for piece in rock.coordinates:
            if piece[1] - 1 < 0 or (piece[1] - 1 >= 0 and grid[piece[0]][piece[1] - 1] != "."):
                return False
    elif direction == ">":
        for piece in rock.coordinates:
            if piece[1] + 1 > 6 or (piece[1] + 1 <= 6 and grid[piece[0]][piece[1] + 1] != "."):
                return False
    else:
        for piece in rock.coordinates:
            if piece[0] - 1 < 0 or (piece[0] - 1 >= 0 and grid[piece[0] - 1][piece[1]] != "."):
                return False
    return True

def settleRock(grid, rock):
    for coordinate in rock.coordinates:
        grid[coordinate[0]][coordinate[1]] = "#"
    return grid

main()