import json
import copy

packets = open("input.txt", "r").read().replace("\n\n", "\n").splitlines()
packets.append("[[6]]")
packets.append("[[2]]")

def createListStructure(packet):
    return json.loads(packet)

def isOrdered(left, right):
    for i in range(len(right)):
        if len(left) == i:
            return True
        if type(left[i]) == list and type(right[i]) == int:
            right[i] = [right[i]]
        elif type(left[i]) == int and type(right[i]) == list:
            left[i] = [left[i]]
        if type(left[i]) == int and type(right[i]) == int:
            if left[i] < right[i]:
                return True
            elif right[i] < left[i]:
                return False
        else:
            ordered = isOrdered(left[i], right[i])
            if ordered != "Undecided":
                return ordered
    if len(right) < len(left):
        return False
    else:
        return "Undecided"

for i in range(len(packets)):
    packets[i] = createListStructure(packets[i])

ordered = False
while ordered == False:
    ordered = True
    for i in range(len(packets) - 1):
        left = packets[i]
        right = packets[i+1]
        order = isOrdered(copy.deepcopy(left), copy.deepcopy(right))
        if not order and not order == "Undecided":
            packets[i] = right
            packets[i+1] = left
            ordered = False

two = -1
six = -1
for i in range(len(packets)):
    if packets[i] == [[2]]:
        two = i + 1
    elif packets[i] == [[6]]:
        six = i + 1
        
print(two * six)