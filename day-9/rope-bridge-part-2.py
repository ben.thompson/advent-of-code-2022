movements = open("input.txt", "r").read().splitlines()

visited = set()
knots = [ [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0] ]
for movement in movements:
    direction, steps = movement.split()
    for i in range(int(steps)):
        if direction == "R":
            knots[0][0] += 1
        elif direction == "L":
            knots[0][0] -= 1
        elif direction == "U":
            knots[0][1] += 1
        elif direction == "D":
            knots[0][1] -= 1
        for knot in range(len(knots) - 1):
            if knots[knot + 1][0] + 2 <= knots[knot][0]:
                knots[knot + 1][0] = knots[knot][0] - 1
                if knots[knot + 1][1] + 1 <= knots[knot][1]:
                    knots[knot + 1][1] += 1
                elif knots[knot + 1][1] - 1 >= knots[knot][1]:
                    knots[knot + 1][1] -= 1
            elif knots[knot + 1][0] - 2 >= knots[knot][0]:
                knots[knot + 1][0] = knots[knot][0] + 1
                if knots[knot + 1][1] + 1 <= knots[knot][1]:
                    knots[knot + 1][1] += 1
                elif knots[knot + 1][1] - 1 >= knots[knot][1]:
                    knots[knot + 1][1] -= 1
            elif knots[knot + 1][1] + 2 <= knots[knot][1]:
                knots[knot + 1][1] = knots[knot][1] - 1
                if knots[knot + 1][0] + 1 <= knots[knot][0]:
                    knots[knot + 1][0] += 1
                elif knots[knot + 1][0] - 1 >= knots[knot][0]:
                    knots[knot + 1][0] -= 1
            elif knots[knot + 1][1] - 2 >= knots[knot][1]:
                knots[knot + 1][1] = knots[knot][1] + 1
                if knots[knot + 1][0] + 1 <= knots[knot][0]:
                    knots[knot + 1][0] += 1
                elif knots[knot + 1][0] - 1 >= knots[knot][0]:
                    knots[knot + 1][0] -= 1
        visited.add(tuple(knots[9]))

print(len(visited))