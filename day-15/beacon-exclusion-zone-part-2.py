import math

sensorsAndBeacons = open("input.txt", "r").read().splitlines()

def main(maxValue):
    for y in range(maxValue + 1):
        signals = findSignals(y)
        signalGap = findSignalGap(signals)
        if signalGap != -1:
            print(signalGap * 4000000 + y)
            return

def findSignals(y):
    signals = []
    for sensorBeaconPair in sensorsAndBeacons:
        sensor = [int(i) for i in sensorBeaconPair.split("Sensor at x=")[1].split(": closest beacon is at")[0].split(", y=")]
        beacon = [int(i) for i in sensorBeaconPair.split(": closest beacon is at x=")[1].split(", y=")]
        signalRange = abs(sensor[0] - beacon[0]) + abs(sensor[1] - beacon[1])
        if y + signalRange >= sensor[1] and y - signalRange <= sensor[1]:
            signalLeft = sensor[0] - signalRange + abs(y - sensor[1])
            signalRight = sensor[0] + signalRange - abs(y - sensor[1])
            signals.append([signalLeft, signalRight])
    return signals

def findSignalGap(signals):
    i = 0
    totalRuns = 0
    maxRuns = int(math.log2(len(signals))) + 1
    while len(signals) > 1 and totalRuns < maxRuns:
        removeItems = []
        j = i + 1
        while j < len(signals):
            if signals[i][0] <= signals[j][0] and signals[i][1] >= signals[j][1]:
                removeItems.append(j)
            elif signals[i][0] <= signals[j][0] and signals[i][1] < signals[j][1] and signals[i][1] >= signals[j][0]:
                signals[i][1] = signals[j][1]
                removeItems.append(j)
            elif signals[i][0] >= signals[j][0] and signals[i][1] <= signals[j][1]:
                signals[i] = signals[j]
                removeItems.append(j)
            elif signals[i][0] >= signals[j][0] and signals[i][1] > signals[j][1] and signals[i][1] >= signals[j][0]:
                signals[i][0] = signals[j][0]
                removeItems.append(j)
            j += 1
        for item in range(len(removeItems) - 1, -1, -1):
            signals.pop(removeItems[item])
        i += 1
        if i == len(signals):
            i = 0
            totalRuns += 1
    if len(signals) == 2:
        return signals[0][1] + 1
    else:
        return -1

main(4000000)