hill = open("input.txt", "r").read().splitlines()

def findPath():
    numSteps = 0
    alreadyTried = [ findStart() ]
    paths = [ [findStart()] ]
    while not isComplete(paths) and numSteps <= 100000:
        numSteps += 1
        newPaths = []
        for path in paths:
            if isTestable(path[-1], (1, 0), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0] + 1, path[-1][1]])
                alreadyTried.append([path[-1][0] + 1, path[-1][1]])
                newPaths.append(newPath)
            if isTestable(path[-1], (0, 1), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0], path[-1][1] + 1])
                alreadyTried.append([path[-1][0], path[-1][1] + 1])
                newPaths.append(newPath)
            if isTestable(path[-1], (-1, 0), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0] - 1, path[-1][1]])
                alreadyTried.append([path[-1][0] - 1, path[-1][1]])
                newPaths.append(newPath)
            if isTestable(path[-1], (0, -1), alreadyTried):
                newPath = path.copy()
                newPath.append([path[-1][0], path[-1][1] - 1])
                alreadyTried.append([path[-1][0], path[-1][1] - 1])
                newPaths.append(newPath)
        paths = newPaths.copy()
    return numSteps

def findStart():
    for row in range(len(hill)):
        if "S" in hill[row]:
            return [row, hill[row].index("S")]
    return [0, 0]

def isComplete(paths):
    for path in paths:
        if hill[path[-1][0]][path[-1][1]] == "E":
            return True
    return False

def isTestable(location, movement, alreadyTried):
    return (
            location[0] + movement[0] >= 0 and location[1] + movement[1] >= 0 and
            len(hill) > location[0] + movement[0] and len(hill[0]) > location[1] + movement[1] and
            [location[0] + movement[0], location[1] + movement[1]] not in alreadyTried and
            isSteppable([location[0], location[1]], [location[0] + movement[0], location[1] + movement[1]])
        )

def isSteppable(location, destination):
    dest = hill[destination[0]][destination[1]]
    if hill[destination[0]][destination[1]] == "E":
        dest = "z"
    source = hill[location[0]][location[1]]
    if hill[location[0]][location[1]] == "S":
        source = "a"
    return ord(dest) - ord(source) >= -10 and ord(dest) - ord(source) <= 1


print(findPath())