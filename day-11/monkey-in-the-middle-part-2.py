class Monkey:
    def __init__(self, items, operation, divisor, sendMonkey1, sendMonkey2):
        self.items = items
        self.operation = operation
        self.divisor = divisor
        self.sendMonkey1 = sendMonkey1
        self.sendMonkey2 = sendMonkey2
        self.itemsInspected = 0

def monkeyBusiness(monkeys):
    monkeyInspections = []
    for monkey in monkeys:
        monkeyInspections.append(monkey.itemsInspected)
    monkeyInspections.sort(reverse=True)
    return monkeyInspections[0] * monkeyInspections[1]

monkeyHabits = open("input.txt", "r").read().split("\n\n")

monkeys = []
for monkeyHabit in monkeyHabits:
    monkeys.append(Monkey(
        items = [int(i) for i in monkeyHabit.split("Starting items: ")[1].split("\n")[0].split(", ")],
        operation = monkeyHabit.split("Operation: new = ")[1].split("\n")[0],
        divisor = int(monkeyHabit.split("Test: divisible by ")[1].split("\n")[0]),
        sendMonkey1 = int(monkeyHabit.split("If true: throw to monkey ")[1].split("\n")[0]),
        sendMonkey2 = int(monkeyHabit.split("If false: throw to monkey ")[1].split("\n")[0]),
    ))

for round in range(10000):
    for monkey in monkeys:
        monkey.itemsInspected += len(monkey.items)
        for item in monkey.items:
            item = eval(monkey.operation.replace("old", str(item))) % 9699690
            if item % monkey.divisor == 0:
                monkeys[monkey.sendMonkey1].items.append(item)
            else:
                monkeys[monkey.sendMonkey2].items.append(item)
        monkey.items = []

print(monkeyBusiness(monkeys))