valveInput = open("input.txt", "r").read().splitlines()

class Valve:
    def __init__(self, rate, nextValves):
        self.rate = rate
        self.nextValves = nextValves

class Path:
    def __init__(self, myValve, elephantValve, currentRate, totalReleased, openedValves):
        self.myValve = myValve
        self.elephantValve = elephantValve
        self.currentRate = currentRate
        self.totalReleased = totalReleased
        self.openedValves = openedValves

    def openValve(self, newValve, newRate):
        self.openedValves.append(newValve)
        self.currentRate += newRate

    def runMinute(self):
        self.totalReleased += self.currentRate

def main():
    valves = createValves()
    paths = [ Path("AA", "AA", 0, 0, []) ]
    for i in range(26):
        newPaths = []
        for path in paths:
            path.runMinute()
        for path in paths:
            if isPressured(path, valves, True):
                newPath = Path(path.myValve, path.elephantValve, path.currentRate, path.totalReleased, path.openedValves.copy())
                newPath.openValve(newPath.myValve, valves[newPath.myValve].rate)
                if isPressured(newPath, valves, False):
                    newPath.openValve(newPath.elephantValve, valves[newPath.elephantValve].rate)
                    newPaths.append(newPath)
                else:
                    for valve in valves[path.elephantValve].nextValves:
                        newPath = Path(newPath.myValve, valve, newPath.currentRate, newPath.totalReleased, newPath.openedValves.copy())
                        newPaths.append(newPath)
            elif isPressured(path, valves, False):
                newPath = Path(path.myValve, path.elephantValve, path.currentRate, path.totalReleased, path.openedValves.copy())
                newPath.openValve(newPath.elephantValve, valves[newPath.elephantValve].rate)
                for myValve in valves[path.myValve].nextValves:
                    newPath = Path(myValve, newPath.elephantValve, newPath.currentRate, newPath.totalReleased, newPath.openedValves.copy())
                    newPaths.append(newPath)
            for myValve in valves[path.myValve].nextValves:
                for elephantValve in valves[path.elephantValve].nextValves:
                    newPath = Path(myValve, elephantValve, path.currentRate, path.totalReleased, path.openedValves.copy())
                    newPaths.append(newPath)
        paths = reducePaths(newPaths)
    bestPath = Path("AA", "AA", 0, 0, [])
    for path in paths:
        if path.totalReleased > bestPath.totalReleased:
            bestPath = path
    print(bestPath.totalReleased)

def createValves():
    valves = {}
    for row in valveInput:
        label = row.split("Valve ")[1][:2]
        rate = int(row.split("rate=")[1].split(";")[0])
        if "tunnels lead to valves" in row:
            nextValves = row.split("tunnels lead to valves ")[1].split(", ")
        else:
            nextValves = [row.split("tunnel leads to valve ")[1]]
        valves[label] = Valve(rate, nextValves)
    return valves

def isPressured(path, valves, isMe):
    if isMe:
        return valves[path.myValve].rate > 0 and path.myValve not in path.openedValves
    else:
        return valves[path.elephantValve].rate > 0 and path.elephantValve not in path.openedValves

def reducePaths(paths):
    paths.sort(key=lambda x: x.totalReleased, reverse=True)
    return paths[:100000]

main()