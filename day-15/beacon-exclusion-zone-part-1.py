sensorsAndBeacons = open("input.txt", "r").read().splitlines()

def main(targetY):
    minX, maxX = findBoundaries()
    targetRow = ["." for i in range(maxX+1 - minX)]
    fillSignal(targetRow, targetY, minX)
    print(countSignal(targetRow))

def findBoundaries():
    minX = 100000
    maxX = -100000
    for sensorBeaconPair in sensorsAndBeacons:
        sensor = [int(i) for i in sensorBeaconPair.split("Sensor at x=")[1].split(": closest beacon is at")[0].split(", y=")]
        beacon = [int(i) for i in sensorBeaconPair.split(": closest beacon is at x=")[1].split(", y=")]
        signalRange = abs(sensor[0] - beacon[0]) + abs(sensor[1] - beacon[1])
        if sensor[0] + signalRange > maxX:
            maxX = sensor[0] + signalRange
        elif sensor[0] - signalRange < minX:
            minX = sensor[0] - signalRange
        if beacon[0] > maxX:
            maxX = beacon[0]
        elif beacon[0] < minX:
            minX = beacon[0]
    return minX, maxX

def fillSignal(targetRow, targetY, minX):
    for sensorBeaconPair in sensorsAndBeacons:
        sensor = [int(i) for i in sensorBeaconPair.split("Sensor at x=")[1].split(": closest beacon is at")[0].split(", y=")]
        beacon = [int(i) for i in sensorBeaconPair.split(": closest beacon is at x=")[1].split(", y=")]
        if beacon[1] == targetY:
            targetRow[beacon[0] - minX] = "B"
        signalRange = abs(sensor[0] - beacon[0]) + abs(sensor[1] - beacon[1])
        if targetY > sensor[1] - signalRange and targetY < sensor[1] + signalRange:
            for i in range(-(signalRange - abs(targetY - sensor[1])), (signalRange - abs(targetY - sensor[1]) + 1)):
                if targetRow[i + sensor[0] - minX] == ".":
                    targetRow[i + sensor[0] - minX] = "#"

def countSignal(row):
    count = 0
    for col in row:
        if col != "." and col != "B":
            count += 1
    return count

main(2000000)