def getPriority(letter):
    if ord(letter) > 96:
        return ord(letter) - 96
    else:
        return ord(letter) - 38

rucksacks = open("input.txt", "r").read().splitlines()
totalPriority = 0
for rucksack in rucksacks:
    half = int(len(rucksack)/2)
    first = rucksack[:half]
    second = rucksack[half:]
    for letter in first:
        if letter in second:
            totalPriority += getPriority(letter)
            break

print(totalPriority)