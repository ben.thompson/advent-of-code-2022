commands = open("input.txt", "r").read().split("$ ")

directories = {}
currentDirectories = []
currentDirectory = ""
for command in commands:
    if command == "":
        continue
    elif command[0:2] == "ls":
        for file in command[3:].splitlines():
            fileParts = file.split(" ")
            if fileParts[0] != "dir":
                for directory in currentDirectories:
                    directories[directory] += int(fileParts[0])
    else:
        commandParts = command.replace("\n", "").split(" ")
        if commandParts[1] == "..":
            currentDirectory = currentDirectory[:currentDirectory.rindex("/")]
            currentDirectories.pop()
        else:
            currentDirectory = currentDirectory + "/" + commandParts[1]
            currentDirectories.append(currentDirectory)
            if commandParts[1] not in directories.keys():
                directories[currentDirectory] = 0

result = 0
for directory in directories.values():
    if directory <= 100000:
        result += directory

print(result)