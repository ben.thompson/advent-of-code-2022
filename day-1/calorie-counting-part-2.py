elfCalories = open("input.txt", "r")
elves = elfCalories.readlines()

highestCalories = [0, 0, 0]
elfCounter = 0
currentElfCalories = 0
for elf in elves:
    if elf == '\n':
        for i in range(len(highestCalories)):
            if currentElfCalories >= highestCalories[i]:
                highestCalories[i] = currentElfCalories
                highestCalories.sort()
                break
        elfCounter += 1
        currentElfCalories = 0
    else:
        currentElfCalories += int(elf)
for i in range(len(highestCalories)):
    if currentElfCalories >= highestCalories[i]:
        highestCalories[i] = currentElfCalories
        break

print(sum(highestCalories))
