valveInput = open("input.txt", "r").read().splitlines()

class Valve:
    def __init__(self, rate, nextValves):
        self.rate = rate
        self.nextValves = nextValves

class Path:
    def __init__(self, currentValve, currentRate, totalReleased, openedValves):
        self.currentValve = currentValve
        self.currentRate = currentRate
        self.totalReleased = totalReleased
        self.openedValves = openedValves

    def openValve(self, newValve, newRate):
        self.openedValves.append(newValve)
        self.currentRate += newRate

    def runMinute(self):
        self.totalReleased += self.currentRate

def main():
    valves = createValves()
    paths = [ Path("AA", 0, 0, []) ]
    for i in range(30):
        newPaths = []
        for path in paths:
            path.runMinute()
        for path in paths:
            if isPressured(path, valves):
                newPath = Path(path.currentValve, path.currentRate, path.totalReleased, path.openedValves.copy())
                newPath.openValve(newPath.currentValve, valves[newPath.currentValve].rate)
                newPaths.append(newPath)
            for valve in valves[path.currentValve].nextValves:
                newPath = Path(valve, path.currentRate, path.totalReleased, path.openedValves.copy())
                newPaths.append(newPath)
        paths = reducePaths(newPaths)
    bestPath = 0
    for path in paths:
        if path.totalReleased > bestPath:
            bestPath = path.totalReleased
    print(bestPath)

def createValves():
    valves = {}
    for row in valveInput:
        label = row.split("Valve ")[1][:2]
        rate = int(row.split("rate=")[1].split(";")[0])
        if "tunnels lead to valves" in row:
            nextValves = row.split("tunnels lead to valves ")[1].split(", ")
        else:
            nextValves = [row.split("tunnel leads to valve ")[1]]
        valves[label] = Valve(rate, nextValves)
    return valves

def isPressured(path, valves):
    return valves[path.currentValve].rate > 0 and path.currentValve not in path.openedValves

def reducePaths(paths):
    paths.sort(key=lambda x: x.totalReleased, reverse=True)
    return paths[:1000]

main()