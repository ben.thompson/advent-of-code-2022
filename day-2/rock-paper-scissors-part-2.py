file = open("input.txt", "r")
lines = file.readlines()
points = 0
for line in lines:
    opponent = line[0]
    outcome = line[2]
    if outcome == "X":
        if opponent == "A":
            points += 3
        elif opponent == "B":
            points += 1
        else:
            points += 2
    elif outcome == "Y":
        if opponent == "A":
            points += 4
        elif opponent == "B":
            points += 5
        else:
            points += 6
    else:
        if opponent == "A":
            points += 8
        elif opponent == "B":
            points += 9
        else:
            points += 7
print(points)
