commands = open("input.txt", "r").read().split("$ ")

directories = {}
currentDirectories = []
currentDirectory = ""
for command in commands:
    if command == "":
        continue
    elif command[0:2] == "ls":
        for file in command[3:].splitlines():
            fileParts = file.split(" ")
            if fileParts[0] != "dir":
                for directory in currentDirectories:
                    directories[directory] += int(fileParts[0])
    else:
        commandParts = command.replace("\n", "").split(" ")
        if commandParts[1] == "..":
            currentDirectory = currentDirectory[:currentDirectory.rindex("/") - 1]
            currentDirectories.pop()
        else:
            if commandParts[1] == "/":
                currentDirectory = "/"
            else:
                currentDirectory = currentDirectory + "/" + commandParts[1]
            currentDirectories.append(currentDirectory)
            if commandParts[1] not in directories.keys():
                directories[currentDirectory] = 0

amountToFreeUp = directories["/"] - 40000000

smallestPossibleDirectorySize = 70000000
for directory in directories.values():
    if directory >= amountToFreeUp and directory < smallestPossibleDirectorySize:
        smallestPossibleDirectorySize = directory

print(smallestPossibleDirectorySize)