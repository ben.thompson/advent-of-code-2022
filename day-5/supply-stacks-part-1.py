def main():
    stackLines, movements = createSplit()
    stackLines.reverse()
    stacks = initializeStacks(stackLines[1:])
    for movement in movements:
        stacks = performMovement(movement, stacks)
    for stack in stacks:
        print(stack[-1], end='')
    print()

def createSplit():
    parts = open("input.txt", "r").read().split("\n\n")
    stackLines = parts[0].splitlines()
    movements = parts[1].splitlines()
    return stackLines, movements

def initializeStacks(lines):
    numColumns = int((len(lines[0])+1) / 4)
    stacks = []
    for i in range(numColumns):
        stacks.append([])
    for line in lines:
        for column in range(numColumns):
            crate = line[column * 4 + 1]
            if crate != " ":
                stacks[column].append(crate)
    return stacks

def performMovement(movement, stacks):
    numbers = movement.split(" ")
    numToMove = int(numbers[1])
    source = int(numbers[3]) - 1
    destination = int(numbers[5]) - 1
    for i in range(numToMove):
        stacks[destination].append(stacks[source].pop())
    return stacks

main()