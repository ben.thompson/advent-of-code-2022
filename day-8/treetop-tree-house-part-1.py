def isVisible(grid, row, col):
    tree = grid[row][col]
    isVisibleNorth = True
    for i in range(0, row):
        if grid[i][col] >= tree:
            isVisibleNorth = False
            break
    if isVisibleNorth:
        return True
    isVisibleSouth = True
    for i in range(row + 1, len(grid)):
        if grid[i][col] >= tree:
            isVisibleSouth = False
            break
    if isVisibleSouth:
        return True
    isVisibleWest = True
    for i in range(0, col):
        if grid[row][i] >= tree:
            isVisibleWest = False
            break
    if isVisibleWest:
        return True
    isVisibleEast = True
    for i in range(col + 1, len(grid[row])):
        if grid[row][i] >= tree:
            isVisibleEast = False
            break
    return isVisibleEast

rows = open("input.txt", "r").read().splitlines()
grid = []
for row in rows:
    grid.append(list(row))

visible = 0
for i in range(len(grid)):
    for j in range(len(grid[i])):
        if isVisible(grid, i, j):
            visible += 1

print(visible)