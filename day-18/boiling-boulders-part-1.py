boulders = open("input.txt", "r").read().splitlines()

def main():
    max = findMax()
    grid = [ [ ["." for i in range(max + 2)] for i in range(max + 2)] for i in range(max + 2) ]
    grid = fillGrid(grid)
    print(countSides(grid))

def findMax():
    max = 0
    for boulder in boulders:
        x, y, z = [int(i) for i in boulder.split(",")]
        if x > max:
            max = x
        if y > max:
            max = y
        if z > max:
            max = z
    return max

def fillGrid(grid):
    for boulder in boulders:
        x, y, z = [int(i) for i in boulder.split(",")]
        grid[x][y][z] = "#"
    return grid

def countSides(grid):
    totalSides = 0
    for i in range(len(grid) - 1):
        for j in range(len(grid[i]) - 1):
            for k in range(len(grid[j]) - 1):
                if grid[i][j][k] == "#":
                    if grid[i + 1][j][k] == ".":
                        totalSides += 1
                    if grid[i - 1][j][k] == ".":
                        totalSides += 1
                    if grid[i][j + 1][k] == ".":
                        totalSides += 1
                    if grid[i][j - 1][k] == ".":
                        totalSides += 1
                    if grid[i][j][k + 1] == ".":
                        totalSides += 1
                    if grid[i][j][k - 1] == ".":
                        totalSides += 1
    return totalSides

main()