movements = open("input.txt", "r").read().splitlines()

visited = [ (0, 0) ]
H = [0, 0]
T = [0, 0]
for movement in movements:
    direction, steps = movement.split()
    for i in range(int(steps)):
        if direction == "R":
            H[0] += 1
        elif direction == "L":
            H[0] -= 1
        elif direction == "U":
            H[1] += 1
        elif direction == "D":
            H[1] -= 1
        if T[0] + 2 <= H[0]:
            T = [ H[0] - 1, H[1] ]
        elif T[0] - 2 >= H[0]:
            T = [ H[0] + 1, H[1] ]
        elif T[1] + 2 <= H[1]:
            T = [ H[0], H[1] - 1 ]
        elif T[1] - 2 >= H[1]:
            T = [ H[0], H[1] + 1 ]
        visited.append(tuple(T))

print(len(set(visited)))