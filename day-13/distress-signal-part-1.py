import json

pairs = open("input.txt", "r").read().split("\n\n")

def createListStructure(packets):
    return json.loads(packets[0]), json.loads(packets[1])

def isOrdered(left, right):
    for i in range(len(right)):
        if len(left) == i:
            return True
        if type(left[i]) == list and type(right[i]) == int:
            right[i] = [right[i]]
        elif type(left[i]) == int and type(right[i]) == list:
            left[i] = [left[i]]
        if type(left[i]) == int and type(right[i]) == int:
            if left[i] < right[i]:
                return True
            elif right[i] < left[i]:
                return False
        else:
            ordered = isOrdered(left[i], right[i])
            if ordered != "Undecided":
                return ordered
    if len(right) < len(left):
        return False
    else:
        return "Undecided"

sumIndices = 0
index = 1
for pair in pairs:
    left, right = createListStructure(pair.splitlines())
    if isOrdered(left, right) or isOrdered(left, right) == "Undecided":
        sumIndices += index
    index += 1

print(sumIndices)