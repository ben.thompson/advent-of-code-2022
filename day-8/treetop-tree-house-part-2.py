def scenicScore(grid, row, col):
    tree = grid[row][col]
    northScore = 0
    for i in range(row-1, -1, -1):
        northScore += 1
        if grid[i][col] >= tree:
            break
    southScore = 0
    for i in range(row + 1, len(grid)):
        southScore += 1
        if grid[i][col] >= tree:
            break
    westScore = 0
    for i in range(col-1, -1, -1):
        westScore += 1
        if grid[row][i] >= tree:
            break
    eastScore = 0
    for i in range(col + 1, len(grid[row])):
        eastScore += 1
        if grid[row][i] >= tree:
            break
    return northScore * southScore * westScore * eastScore

rows = open("input.txt", "r").read().splitlines()
grid = []
for row in rows:
    grid.append(list(row))

bestScenicScore = 0
for i in range(len(grid)):
    for j in range(len(grid[i])):
        currentScenicScore = scenicScore(grid, i, j)
        if currentScenicScore > bestScenicScore:
            bestScenicScore = currentScenicScore

print(bestScenicScore)