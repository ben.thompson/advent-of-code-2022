elfCalories = open("input.txt", "r")
elves = elfCalories.readlines()

highestCalories = 0
elfCounter = 0
currentElfCalories = 0
for elf in elves:
    if elf == '\n':
        if currentElfCalories >= highestCalories:
            highestCalories = currentElfCalories
        elfCounter += 1
        currentElfCalories = 0
    else:
        currentElfCalories += int(elf)

print(highestCalories)
