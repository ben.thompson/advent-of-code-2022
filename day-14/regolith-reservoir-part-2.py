rockSections = open("input.txt", "r").read().splitlines()

def main():
    maxY = findBoundaries()
    grid = [["." for i in range((maxY+3) * 2 + 1)] for j in range(maxY+3)]
    grid = fillGrid(grid, 497-maxY)
    nextSand = pourSand(grid, 497-maxY)
    while nextSand != (0, maxY+3):
        grid[nextSand[0]][nextSand[1]] = "o"
        nextSand = pourSand(grid, 497-maxY)
    print(countSand(grid))

def findBoundaries():
    maxY = 0
    for rockSection in rockSections:
        for rock in rockSection.split(" -> "):
            y = int(rock.split(",")[1])
            if y > maxY:
                maxY = y
    return maxY

def fillGrid(grid, offsetX):
    grid[0][500-offsetX] = "+"
    for rockSection in rockSections:
        rocks = rockSection.split(" -> ")
        for rock in range(len(rocks) - 1):
            sourceX, sourceY = [int(i) for i in rocks[rock].split(",")]
            destX, destY = [int(i) for i in rocks[rock+1].split(",")]
            if sourceX == destX:
                if sourceY < destY:
                    start = sourceY
                    end = destY
                else:
                    start = destY
                    end = sourceY
                for i in range(start, end + 1):
                    grid[i][sourceX-offsetX] = "#"
            else:
                if sourceX < destX:
                    start = sourceX
                    end = destX
                else:
                    start = destX
                    end = sourceX
                for i in range(start - offsetX, end - offsetX + 1):
                    grid[sourceY][i] = "#"
    grid[-1] = ["#" for i in range(len(grid[-1]))]
    return grid

def pourSand(grid, offsetX):
    row = 0
    col = 500 - offsetX
    while True:
        if row + 1 >= len(grid):
            return None
        elif grid[row+1][col] == ".":
            row += 1
        elif col-1 < 0:
            return None
        elif grid[row+1][col-1] == ".":
            row += 1
            col -= 1
        elif col+1 >= len(grid[0]):
            return None
        elif grid[row+1][col+1] == ".":
            row += 1
            col += 1
        else:
            return (row, col)

def countSand(grid):
    sand = 1
    for row in grid:
        sand += row.count("o")
    return sand

main()